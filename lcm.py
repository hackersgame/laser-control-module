#!/usr/bin/python3
import time
import pigpio

import RPi.GPIO as GPIO
import serial
ser = serial.Serial ("/dev/ttyAMA0")
ser.baudrate = 115200 
CMDs_START_WITH = ['x', 'y', 'p']

pi = pigpio.pi()
server_pin_x = 18 #pin 12
server_pin_y = 19 #pin 35
laserPIN =        16 #pin 36

GPIO.setmode(GPIO.BCM)
GPIO.setup(laserPIN, GPIO.OUT)
GPIO.output(laserPIN, GPIO.HIGH)


next_pos = [1555,1555]
#hw_limits = [[500,1983],[1359,2500]]
hw_limits = [[1000,2000],[1000,2000]]


def return_pwd(angle):
    angle = float(angle)
    return (int((angle / (90/1000)) + 1000))

def return_angle():
    global next_pos
    global ser
    #TODO convert to angle
    x = next_pos[0]
    y = next_pos[1]
    
    #1000 = 0 (down)
    #2000 = 90 (forward)
    x_angle = (90/1000) * (x - 1000)
    y_angle = ((90/1000) * (y - 1000))
    
    ser.write(f"x:{x_angle:.2f}\ny:{y_angle:.2f}\n".encode())

#Ex CMD to chage PWD on X by 20
#x:20
#Ex CMD to chage x min max
#x~20.1:70
def process(cmd):
    global next_pos
    global hw_limits
    
    if '~' in cmd:
        set_new_limit = False
        what, new_max = cmd.split('~')
        new_max = new_max.split(':')
        new_max = [return_pwd(new_max[0]),return_pwd(new_max[1])]
        #print("Debug: " + str(new_max) + " " + str(what))
        
        if what == 'x':
            if new_max[0] > hw_limits[0][0]:
                hw_limits[0][0] =  new_max[0]
                set_new_limit = True
            if  new_max[1] < hw_limits[0][1]:
                hw_limits[0][1] =  new_max[1]
                set_new_limit = True
        if what == 'y':
            if new_max[0] > hw_limits[1][0]:
                hw_limits[1][0] =  new_max[0]
                set_new_limit = True
            if  new_max[1] < hw_limits[1][1]:
                hw_limits[1][1] =  new_max[1]
                set_new_limit = True
        if set_new_limit:
            print("Set new min_max")
            print(hw_limits)
        return()
    
    if ':' in cmd:
        what, power = cmd.split(':')
        power = int(power)
        print([what, power])

    new_x , new_y = next_pos
    if what == 'x':
        #set x to a max of  hw_limits[0][1]
        new_x = next_pos[0] + power
        if new_x > hw_limits[0][1]:
            new_x = hw_limits[0][1]
        #set x to a min of  hw_limits[0][0]
        if new_x < hw_limits[0][0]:
            new_x = hw_limits[0][0]
    if what == 'y':
        #set y to a max of  hw_limits[1][1]
        new_y = next_pos[1] + power
        if new_y > hw_limits[1][1]:
            new_y = hw_limits[1][1]
        #set y to a min of  hw_limits[1][0]
        if new_y < hw_limits[1][0]:
            new_y = hw_limits[1][0]
    
    next_pos =[new_x , new_y]
    print("SET: " +str(next_pos))
    pi.set_servo_pulsewidth(server_pin_x, int(next_pos[0]))
    pi.set_servo_pulsewidth(server_pin_y, int(next_pos[1]))
    return_angle()
    
cmd_buffer = ""
while True:
    for data in ser.read():
        cmd_buffer = cmd_buffer + chr(data)
        #print(cmd_buffer + "Buffer only..." + str(data))
        if data == 13 or data == 10: #\n
            if cmd_buffer[0] in CMDs_START_WITH:
                try:
                    process(cmd_buffer[:-1])
                except Exception as e:
                    print(e)
            cmd_buffer = ""

ser.close()   
